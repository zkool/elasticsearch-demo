package com.elasticsearch.action;

import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;

import com.elasticsearch.config.ElasticsearchUtil;
import com.elasticsearch.pojo.Pager;
import com.opensymphony.xwork2.ActionSupport;

public class SearchAction extends ActionSupport {
	private static final long serialVersionUID = 1L;
	/** 关键字 **/
	private String wd;
	/** 消耗时间 **/
	private double spendTime;
	/** 查询结果集对象 **/
	private List<Map<String, Object>> pageList = new ArrayList<Map<String, Object>>();
	/** 分页对象 **/
	private Pager pager;
	/** 总记录数 使用静态变量的方式缓存 **/
	private Long total;

	private String keyWord;

	private SearchResponse response;

	/**
	 * 条件检索action
	 * 
	 * @throws MalformedURLException
	 * @throws SolrServerException
	 * @throws UnsupportedEncodingException
	 **/
	public String search() throws MalformedURLException,
			UnsupportedEncodingException {
		/** 检索开始时间 **/
		long startTime = System.nanoTime();

		/** 获取页面封装好的分页对象 **/
		if (pager == null) {
			pager = new Pager();
			pager.setMaxPageItems(10);
		}
		
		pager.setDefaultMaxPageItems(1);
		/**高亮字段**/
		String[] highFields=new String[]{"DESCRIPTION","TITLE"};
		
		response = ElasticsearchUtil.searcher("jdbc", "jdbc",
				pager.getOffset(), pager.getMaxPageItems(), wd,highFields);

		/** 总记录数 **/
		total = response.getHits().totalHits();

		System.out.println("命中总数：" + total);
		SearchHits searchHits = response.getHits();
		SearchHit[] hits = searchHits.getHits();
		for (int i = 0; i < hits.length; i++) {
			Map<String, Object> map = new HashMap<String, Object>();
			SearchHit hit = hits[i];
			String id=hit.getId();
			
			String content = ElasticsearchUtil.getHighlightFields(hit,"DESCRIPTION");
			String title = ElasticsearchUtil.getHighlightFields(hit,"TITLE");
		
			map.put("id", hit.getSource().get("FEED_ID"));
			map.put("content", content);
			map.put("title", title);
			//map.put("title", hit.getSource().get("title"));
			//map.put("create_time", hit.getSource().get("create_time"));
			map.put("links", hit.getSource().get("LINK"));
			// Map result = hit.getSource();
			// System.out.println(result);
			pageList.add(map);
		}

		/** 检索完成时间 **/
		long endTime = System.nanoTime();
		/** 检索花费时间 **/
		spendTime = (double) (endTime - startTime) / 1000;

		return SUCCESS;
	}

	public static String Html2Text(String inputString) {
		String htmlStr = inputString; // 含html标签的字符串
		String textStr = "";
		java.util.regex.Pattern p_script;
		java.util.regex.Matcher m_script;
		java.util.regex.Pattern p_style;
		java.util.regex.Matcher m_style;
		java.util.regex.Pattern p_html;
		java.util.regex.Matcher m_html;

		try {
			String regEx_script = "<[\\s]*?script[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?script[\\s]*?>"; // 定义script的正则表达式{或<script[^>]*?>[\\s\\S]*?<\\/script>
																										// }
			String regEx_style = "<[\\s]*?style[^>]*?>[\\s\\S]*?<[\\s]*?\\/[\\s]*?style[\\s]*?>"; // 定义style的正则表达式{或<style[^>]*?>[\\s\\S]*?<\\/style>
																									// }
			String regEx_html = "<[^>]+>"; // 定义HTML标签的正则表达式

			p_script = Pattern.compile(regEx_script, Pattern.CASE_INSENSITIVE);
			m_script = p_script.matcher(htmlStr);
			htmlStr = m_script.replaceAll(""); // 过滤script标签

			p_style = Pattern.compile(regEx_style, Pattern.CASE_INSENSITIVE);
			m_style = p_style.matcher(htmlStr);
			htmlStr = m_style.replaceAll(""); // 过滤style标签

			p_html = Pattern.compile(regEx_html, Pattern.CASE_INSENSITIVE);
			m_html = p_html.matcher(htmlStr);
			htmlStr = m_html.replaceAll(""); // 过滤html标签

			textStr = htmlStr;

		} catch (Exception e) {
			System.err.println("Html2Text: " + e.getMessage());
		}

		return textStr;// 返回文本字符串
	}

	public String getWd() {
		return wd;
	}

	public void setWd(String wd) {
		this.wd = wd;
	}

	public double getSpendTime() {
		return spendTime;
	}

	public void setSpendTime(double spendTime) {
		this.spendTime = spendTime;
	}

	public List<Map<String, Object>> getPageList() {
		return pageList;
	}

	public void setPageList(List<Map<String, Object>> pageList) {
		this.pageList = pageList;
	}

	public Pager getPager() {
		return pager;
	}

	public void setPager(Pager pager) {
		this.pager = pager;
	}

	public Long getTotal() {
		return total;
	}

	public void setTotal(Long total) {
		this.total = total;
	}

	public String getKeyWord() {
		return keyWord;
	}

	public void setKeyWord(String keyWord) {
		this.keyWord = keyWord;
	}
}
