package com.elasticsearch.util;

import java.util.List;

import com.elasticsearch.config.ElasticsearchUtil;
import com.elasticsearch.dbuitl.OracleDBCOperation;
import com.elasticsearch.pojo.FeedFavorite;

/**
 * 功能：读取数据库数据创建索引文件 作者：kangjie 时间：2011-9-28下午01:30:01
 */
public class CreatIndexOracle {

	/** 每次提交多少条数据记录 **/
	public static int commitMax = 1000;

	public static void main(String[] args) {
		// CreatIndex field = new CreatIndex();
		// field.addField();

		for (int i = 0; i < 4; i++) {
			ChildThread childThread = new ChildThread(i);
			childThread.start();
		}

	}

	public static class ChildThread extends Thread {
		int number;
		public ChildThread(int number) {
			this.number = number;
		}

		@Override
		public void run() {
			addField();
		}

		/**
		 * 功能：从数据库读取数据批量创建索引 作者：kangjie 时间：2011-9-28下午01:49:51
		 */
		@SuppressWarnings({ "unchecked" })
		public void addField() {
			/** 清除搜索服务器所有索引 **/
			// solrServer.deleteByQuery("*:*");
			/** 生成索引开始时间 **/
			long startTime = System.currentTimeMillis();
			/** 数据库工具类 **/
			OracleDBCOperation db = new OracleDBCOperation();
			/** 数据库总记录数 **/
			long count = db
					.totalRecord("SELECT COUNT(1) FROM TF_GOODS_TEMP_NEW");
			/** 每500提交一次后重新实体化集合对象 **/
			List<FeedFavorite> list = null;
			/** 用来标识当前id **/
			long lastIndex = 0;
			
			int j =0;
			/** 非空判断 **/
			if (count > 0) {
				System.out.println("总记录数：" + count);
				/** 循环读取数据 **/
//				for (long i = 0; i < count; i++) {
//					/** 查询所有记录 **/
//					StringBuffer sql = new StringBuffer(
//							"SELECT feed_id,TITLE,LINK,DESCRIPTION,create_time FROM an_feed_favorite LIMIT ");
//					/** 每commitMax提交一次后重新实体化集合对象 **/
//					list = new ArrayList<FeedFavorite>();
//					/** 当记录数为commitMax倍数时并且i不为0时查询其中commitMax条记录 **/
//					if (i % commitMax == 0 && i != 0) {
//						list = new ArrayList<FeedFavorite>();
//						sql.append(i - commitMax + "," + commitMax);
//						System.out.println("执行sql:" + sql.toString());
//						/** 查询所有记录 **/
//						list = db.dbExecuteQuely(sql.toString());
//						exeCreateIndex(list, "medcl", "news");
//						lastIndex = i;
//						/** 当剩余的记录数不足定义批量提交数时 **/
//						if ((count - i) < commitMax) {
//							break;
//						}
//					}
//				}
//
//				/** 当余下记录数不足commitMax提交剩下的记录 **/
//				StringBuffer sql = new StringBuffer(
//						"SELECT feed_id,TITLE,LINK,DESCRIPTION,create_time FROM an_feed_favorite LIMIT ");
//				list = new ArrayList<FeedFavorite>();
//				sql.append(lastIndex + "," + count);
//				System.out.println("最后执行sql:" + sql.toString());
//				/** 查询所有剩余记录 **/
//				list = db.dbExecuteQuely(sql.toString());
//				/** 创建索引 **/
//				exeCreateIndex(list, "medcl", "news");
				
				if(j<count){
				/** 查询所有记录 **/
				StringBuffer sql = new StringBuffer(
						"SELECT feed_id,TITLE,LINK,DESCRIPTION,create_time FROM an_feed_favorite LIMIT ");
				sql.append(j + "," + j+200);
				j=j+200;
				
				System.out.println("执行sql:" + sql.toString());
				list = db.dbExecuteQuely(sql.toString());
				/** 创建索引 **/
				exeCreateIndex(list, "medcl", "news");
				}
			}
			/** 生成索引结束时间 **/
			long endTime = System.currentTimeMillis();
			System.out.println("生成索引时间:" + (endTime - startTime) / 1000 + "秒");
		}

		/***
		 * 功能：索引生成公共方法 作者：kangjie 时间：2011-9-30下午02:12:55
		 */
		public void exeCreateIndex(List<FeedFavorite> list, String indexName,
				String typeName) {
			/** 判断集合是否为空 **/
			if (list != null && list.size() > 0) {
				/** 集合对象用于批量加入文件数据生成索引文件 **/
				ElasticsearchUtil.createIndex(list, indexName, typeName);
				// SearchService searchService=new SearchServiceImpl();
				// HashMap<String, Object[]> insertContentMap=new
				// HashMap<String, Object[]>();
				// insertContentMap.put(randomString(10), list.toArray());
				// searchService.bulkInsertData(indexName, list);
				try {
					// 防止出现：远程主机强迫关闭了一个现有的连接
					Thread.sleep(3000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
