package com.elasticsearch.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import net.sourceforge.pinyin4j.format.HanyuPinyinVCharType;

public class SpellingTest {
	private static Map<String, String> map = new HashMap<String, String>();

	private static Map<String, Set<String>> spellingMap = new HashMap<String, Set<String>>();

	public static void main(String[] args) throws Exception {
		readFile();
		// Set<String> set=spellingMap.keySet();
		// for(String str: set){
		// System.out.println(spellingMap.get(str));
		// }
		System.out.println(spellingMap.get("zhongguo"));
	}

	public static void readFile() throws Exception {
		loadMainDict();
	}

	private static void loadMainDict() throws Exception {

		InputStream is = new SpellingTest().getClass().getClassLoader()
				.getResourceAsStream("main.dic");
		if (is == null) {
			throw new RuntimeException("Main Dictionary not found!!!");
		}

		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(is,
					"UTF-8"), 512);
			String theWord = null;
			HanyuPinyinOutputFormat format = new HanyuPinyinOutputFormat();
			format.setCaseType(HanyuPinyinCaseType.LOWERCASE);
			format.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
			format.setVCharType(HanyuPinyinVCharType.WITH_V);
			do {
				theWord = br.readLine();
				if (theWord != null && !"".equals(theWord.trim())) {
					char[] temps = theWord.trim().toLowerCase().toCharArray();
					StringBuffer sb = new StringBuffer();
					for (char ch : temps) {
						if (CharacterUtil.identifyCharType(ch) == CharacterUtil.CHAR_CHINESE) {
							String[] ss = PinyinHelper
									.toHanyuPinyinStringArray(ch, format);
							if (ss != null && ss.length > 0) {
								sb.append(ss[0]);
							}
						} else {
							sb.append(ch);
						}
					}
					map.put(theWord, sb.toString());
					Set<String> set = spellingMap.get(sb.toString());
					if (set == null || set.size() == 0) {
						set = new HashSet<String>();
						set.add(theWord);
					} else {
						set.add(theWord);
					}
					spellingMap.put(sb.toString(), set);
				}
			} while (theWord != null);

		} catch (IOException ioe) {
			System.err.println("Main Dictionary loading exception.");
			ioe.printStackTrace();

		} finally {
			try {
				if (is != null) {
					is.close();
					is = null;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
}