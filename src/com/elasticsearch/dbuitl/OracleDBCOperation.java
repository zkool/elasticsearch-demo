package com.elasticsearch.dbuitl;

import java.sql.*;
import java.util.*;

import com.elasticsearch.pojo.FeedFavorite;

/***
 * 功能：数据库操作类 作者：kangjie 时间：2011-9-22下午01:22:49
 */
public class OracleDBCOperation {
	private String dbHost = "localhost"; // 数据库地址
	private String dbSocket = "3306"; // 数据库端口
	private String dbUser = "scwy"; // 数据库用户名
	private String dbPwd = "scwy123"; // 数据库用户密码
	private String dbName = "anynote"; // 使用的数据库
	private Connection dbCon; // 数据库连接
	private Statement dbSta; // 数据库指令集

	/** 获取数据库连接 **/
	private boolean dbOpen() { // 创建数据库连接和指令集
		String url = "jdbc:oracle:thin:@133.37.60.29:1521:wydb";
		try {
			Class.forName("oracle.jdbc.driver.OracleDriver").newInstance(); // 创建mysql驱动
			try {
				this.dbCon = DriverManager.getConnection(url, this.dbUser,
						this.dbPwd); // 创建数据库连接对象
				this.dbSta = this.dbCon.createStatement(); // 根据连接对象创建指令集
				// this.dbSta.execute("use " + this.dbName);
				// System.out.println("数据库连接成功");
				return true; // 返回true
			} catch (Exception ex1) {
				System.out.println("数据库连接错误");
				ex1.printStackTrace();
			}
		} catch (ClassNotFoundException ex) {
			ex.printStackTrace();
		} catch (IllegalAccessException ex) {
			ex.printStackTrace();
		} catch (InstantiationException ex) {
			ex.printStackTrace();
		}
		return false; // 出现异常则返回false

	}

	private boolean dbClose() { // 关闭数据库连接和指令集
		try {
			this.dbSta.close();
			this.dbCon.close();
		} catch (Exception ex) {
			System.out.println("数据库关闭失败！");
			ex.printStackTrace();
		}

		return false;
	}

	public boolean dbExecute(String sql) { // 操作不需要返回数据集的查询语句
		this.dbOpen();
		try {
			this.dbSta.execute(sql); // 执行传递过来的sql语句
			return true;
		} catch (SQLException ex) {
			System.out.println("查询操作错误，查询语句为：");
			System.out.println(sql);
			System.out.println("异常为：");
			ex.printStackTrace();
		} finally {
			this.dbClose();
		}
		return false;
	}

	/***
	 * 功能：执行数据库查询 作者：kangjie 时间：2011-9-22下午01:27:21
	 */
	@SuppressWarnings("unchecked")
	public List dbExecuteQuely(String sql) { // 操作需要返回数据集的查询语句
		this.dbOpen();
		try {
			ResultSet dbRs = this.dbSta.getResultSet(); // 创建数据集
			dbRs = this.dbSta.executeQuery(sql); // 将执行sql语句返回的数据集赋值给dbRs
			List dbList = new ArrayList();
			while (dbRs.next()) { // 遍历行数
				FeedFavorite f = new FeedFavorite();
				
				//Map<String,Object> map=new HashMap<String, Object>();
				//f.setId(dbRs.getInt("feed_id"));
				f.setContent(dbRs.getString("DESCRIPTION"));
                f.setTitle(dbRs.getString("title"));
                f.setLinks(dbRs.getString("link"));
               // f.setCreate_time(dbRs.getDate("create_time"));
//				map.put("feed_id", dbRs.getInt("feed_id"));
//				map.put("description", dbRs.getString("DESCRIPTION"));
//				map.put("title", dbRs.getString("title"));
//				map.put("link", dbRs.getString("link"));
				dbList.add(f); // 将一行数据添加为数组列表一个元素
			}
			dbRs.close(); // 清除数据库结果集
			return dbList; // 返回ArrayList对象
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.dbClose();
		}
		return null; // 出现异常返回null
	}
	
	
	

	/** 总记录数 **/
	public long totalRecord(String sql) {
		this.dbOpen();
		try {
			long total = 0;
			ResultSet dbRs = this.dbSta.getResultSet(); // 创建数据集
			dbRs = this.dbSta.executeQuery(sql); // 将执行sql语句返回的数据集赋值给dbRs
			while (dbRs.next()) { // 遍历行数
				total = dbRs.getLong(1);
			}
			dbRs.close(); // 清除数据库结果集
			return total;
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			this.dbClose();
		}
		return 0;
	}

//	 @SuppressWarnings("unchecked")
//	 public static void main(String[] args) {
//	 DBCOperation test = new DBCOperation();
//	
//	 ArrayList testList = test
//	 .dbExecuteQuely("select FEED_ID,TITLE,LINK,DESCRIPTION,CREATE_TIME from an_feed_favorite limit 0,10");
//	 System.out.println(test
//	 .totalRecord("select count(FEED_ID)  from an_feed_favorite"));
//	 for (int i = 0; i < testList.size(); i++) {
//	 String[] testStr = (String[]) testList.get(i);
//	 for (int j = 0; j < testStr.length; j++) {
//	 System.out.println(testStr[j]);
//	 }
//	 }
//	 }
}
